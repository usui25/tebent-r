Devise::Async.setup do |config|
  config.enabled = true
  config.backend = :sidekiq
  config.queue   = :mailer
  # config.priority = 10
end