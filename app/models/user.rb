class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,  
         :async, :confirmable

  validate :email_is_unique, on: :create
  validate :subdomain_is_unique, on: :create
  after_validation :create_tenant
  after_create :create_account

  # def confirmation_required?
  #   false
  # end
  

  private 

  #Unique email in the account model
  def email_is_unique
    #Don't validate email if errors present
    return false unless self.errors[:email].empty?

    unless Account.find_by_email(email).nil?
      errors.add(:email, " is already used")
    end

  end

  #Subdomains should be unique
  def subdomain_is_unique
    if subdomain.present?
      unless Account.find_by_subdomain(subdomain).nil?
        errors.add(:subdomain,"is already used")
      end
    end
  end

  def create_account
    account = Account.new(email: email, subdomain: subdomain)
    account.save!
  end

  def create_tenant
    #Create Tenant only if it is a new record
    if self.new_record?
      Apartment::Tenant.create(subdomain)
    end
    #Change schema to the tenant
    Apartment::Tenant.switch!(subdomain)
  end
end
