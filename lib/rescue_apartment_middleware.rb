module RescueApartmentMiddleware
  def call(*args)
    begin

      #Call Apartment Middleware to do it's job
      super
    rescue Apartment::TenantNotFound => e
      ap 'TenanNotFound'
      ap e.message
      return [404,{"Content-Type" => "text/html"},
                ["#{File.read(Rails.root.to_s + '/public/404.html')}"]]
    end

  end


end